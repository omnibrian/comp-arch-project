--------------------------------------------------------
-- SSimple Computer Architecture
--
-- memory 256*16
-- 8 bit address; 16 bit data
-- memory.vhd
--------------------------------------------------------

library	ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;   
use work.MP_lib.all;

entity memory is
port ( 	clock	: 	in std_logic;
		rst		: 	in std_logic;
		Mre		:	in std_logic;
		Mwe		:	in std_logic;
		address	:	in std_logic_vector(7 downto 0);
		data_in	:	in std_logic_vector(15 downto 0);
		data_out:	out std_logic_vector(15 downto 0)
);
end memory;

architecture behv of memory	 is			

type ram_type is array (0 to 255) of std_logic_vector(15 downto 0);
signal tmp_ram: ram_type;
begin
	write: process(clock, rst, Mre, address, data_in)
	begin				-- program to generate 10 fabonacci number	 
		if rst='1' then		
			tmp_ram <= (
							0 => x"3002",	   	-- R0 <- #2			mov R0, #2
							1 => x"3102",			-- R1 <- #2			mov R1, #2
							2 => x"3252",			-- R2 <- 0x52		mov R2, 0x52
							3 => x"3301",			-- R3 <- #1			mov R3, #1
							--=> x"3856",			-- R8 <- 0x56		mov R8, 0x56
							4 => x"1350",			-- M[0x50] <- R3 	mov M[0x50], R3
							5 => x"1151",			-- M[0x51] <- R1 	mov M[0x51], R1
							6 => x"4131",			-- R1 <- R1 + R3	add R1, R3
							7 => x"8010",			-- R0 <- R0 * R1	mult R0, R1
							8 => x"2200",			-- M[R2] <- R0 	mov M[R2], R0
							9 => x"4232",			-- R2 <- R2 + R3 	add R2, R3
							10 => x"0456",			-- R4 <- M[0x56]	mov R4, M[0x56]
							-- => x"9480",			-- R4 <- M[R8]		mov R4, M[R8]
							11 => x"6406",  		-- R4=0: PC<- #6	jz R4, #6
							-- => x"6407",			-- R4=0: PC<- #7	jz R4, #7
							
							12 => x"7050",			-- output<- M[0x50]   mov obuf_out,M[0x50]
							13 => x"7051",			-- output<- M[0x51]   mov obuf_out,M[0x51]
							14 => x"7052",			-- output<- M[0x52]   mov obuf_out,M[0x52]
							15 => x"7053",			-- output<- M[0x53]   mov obuf_out,M[0x53]
							16 => x"7054",			-- output<- M[0x54]   mov obuf_out,M[0x54]
							17 => x"7055",			-- output<- M[0x55]   mov obuf_out,M[0x55]
							18 => x"7056",			-- output<- M[0x56]   mov obuf_out,M[0x56]		
							19 => x"F000",			-- halt

--							0 => x"3001",	   	-- R0 <- #1	
--							1 => x"3102",			-- R1 <- #2
--							2 => x"3252",			-- R2 <- #82
--							3 => x"3303",			-- R3 <- #3 
--							4 => x"1050",			-- M[80] <- R0
--							5 => x"1151",			-- M[81] <- R1
--							6 => x"8314",			-- R4 <- R3 * R1
--							7 => x"2240",			-- M[R2] <- R4
--							8 => x"4303",			-- R3 <- R3 + R0
--							9 => x"4202",			-- R2 <- R2 + R0
--							10 => x"1464",			-- M[100] <- R4
--							11 => x"0164",			-- R1 <- M[100]
--							12 => x"0556",			-- R5 <- M[86]
--							13 => x"6506",  		-- R5=0: PC <- #6
--					
--							14 => x"7050",			-- output<- M[80]   mov obuf_out,M[80]
--							15 => x"7051",			-- output<- M[81]   mov obuf_out,M[81]
--							16 => x"7052",			-- output<- M[82]   mov obuf_out,M[82]
--							17 => x"7053",			-- output<- M[83]   mov obuf_out,M[83]
--							18 => x"7054",			-- output<- M[84]   mov obuf_out,M[84]
--							19 => x"7055",			-- output<- M[85]   mov obuf_out,M[85]
--							20 => x"7056",			-- output<- M[86]   mov obuf_out,M[86]			
--							21 => x"F000",			-- halt
							others => x"0000");
		else
			if (clock'event and clock = '1') then
				if (Mwe ='1' and Mre = '0') then
					tmp_ram(conv_integer(address)) <= data_in;
				end if;
			end if;
		end if;
	end process;

    read: process(clock, rst, Mwe, address)
	begin
		if rst='1' then
			data_out <= ZERO;
		else
			if (clock'event and clock = '1') then
				if (Mre ='1' and Mwe ='0') then								 
					data_out <= tmp_ram(conv_integer(address));
				end if;
			end if;
		end if;
	end process;
end behv;