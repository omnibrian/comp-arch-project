------------------------------------------------------------------
-- Cache Controller
--
-- Cache controller for interfacing in between memory and CPU
-- CacheController.vhd
-------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;          
use work.MP_lib.all;

entity CacheController is
port(
    clock       : in std_logic;
    reset       : in std_logic;
    Mre         : in std_logic;
    Mwe         : in std_logic;
    address     : in std_logic_vector(11 downto 0);
    data_in     : in std_logic_vector(15 downto 0);
    read_done   : out std_logic;
    write_done  : out std_logic;
    read_call   : out std_logic;
    write_call  : out std_logic;
    data_out    : out std_logic_vector(15 downto 0);
    d_addr      : out std_logic_vector(8 downto 0);
    d_word      : out std_logic_vector(2 downto 0)
);
end CacheController;

architecture rtl of CacheController is

type read_state_type is (S0,S1,S2,S3);
signal read_state: read_state_type := S0;
type write_state_type is (S0,S1,S2,S3);
signal write_state: write_state_type := S0;
signal tmp_word: integer := 0;

-- Cache storage arrays
type cache_type is array (0 to 7) of std_logic_vector(127 downto 0);
signal tmp_cache: cache_type;
type tag_type is array (0 to 7) of std_logic_vector(6 downto 0);
signal tmp_tag: tag_type;
signal initialized: std_logic_vector(0 to 7);
signal dirty: std_logic_vector(0 to 7);
signal lru: std_logic_vector(0 to 7);

-- Memory signals
signal read_out: std_logic;
signal write_out: std_logic;
signal mem_read: std_logic;
signal mem_write: std_logic;
signal mem_addr: std_logic_vector(8 downto 0);
signal mem_din: std_logic_vector(127 downto 0);
signal mem_dout: std_logic_vector(127 downto 0);

begin

Unit0: MemoryController port map(clock,reset,mem_read,mem_write,mem_addr,mem_din,read_out,write_out,mem_dout);

readwrite: process(clock, reset, Mre, Mwe, read_out, write_out)
variable set: integer;
begin
    if (reset = '1') then
        data_out <= ZERO;
        read_done <= '0';
        mem_read <= '0';
        read_state <= S0;
        write_state <= S0;
        initialized <= "00000000";
        dirty <= "00000000";
    else
        if (clock'event and clock = '1') then
            -- ---------------- --
            -- Read from memory --
            -- ---------------- --
            if (Mre = '1' and Mwe = '0') then -- Read prioritized over write
                -- Reset write state
                write_state <= S0;
                
                case read_state is
                -- ------------------------------------------- --
                -- state 0: initialize and check for cache hit --
                -- ------------------------------------------- --
                when S0 =>
                    if (read_out = '0' and write_out = '0') then
                        set := conv_integer(address(4 downto 3)) * 2;
                        mem_addr <= address(11 downto 3);
                        
                        -- Check for a cache hit
                        if (address(11 downto 5) = tmp_tag(set) and initialized(set) = '1') then
                            case address(2 downto 0) is
                            when "000" =>
                                data_out <= tmp_cache(set) (127 downto 112);
                            when "001" =>
                                data_out <= tmp_cache(set) (111 downto 96);
                            when "010" =>
                                data_out <= tmp_cache(set) (95 downto 80);
                            when "011" =>
                                data_out <= tmp_cache(set) (79 downto 64);
                            when "100" =>
                                data_out <= tmp_cache(set) (63 downto 48);
                            when "101" =>
                                data_out <= tmp_cache(set) (47 downto 32);
                            when "110" =>
                                data_out <= tmp_cache(set) (31 downto 16);
                            when "111" =>
                                data_out <= tmp_cache(set) (15 downto 0);
                            end case;
                            
                            lru(set) <= '1';
                            lru(set + 1) <= '0';
                            
                            read_done <= '1';
                            
                            read_state <= S3;
                        elsif (address(11 downto 5) = tmp_tag(set + 1) and initialized(set + 1) = '1') then
                            case address(2 downto 0) is
                            when "000" =>
                                data_out <= tmp_cache(set + 1) (127 downto 112);
                            when "001" =>
                                data_out <= tmp_cache(set + 1) (111 downto 96);
                            when "010" =>
                                data_out <= tmp_cache(set + 1) (95 downto 80);
                            when "011" =>
                                data_out <= tmp_cache(set + 1) (79 downto 64);
                            when "100" =>
                                data_out <= tmp_cache(set + 1) (63 downto 48);
                            when "101" =>
                                data_out <= tmp_cache(set + 1) (47 downto 32);
                            when "110" =>
                                data_out <= tmp_cache(set + 1) (31 downto 16);
                            when "111" =>
                                data_out <= tmp_cache(set + 1) (15 downto 0);
                            end case;
                            
                            lru(set) <= '0';
                            lru(set + 1) <= '1';
                            
                            read_done <= '1';
                            
                            read_state <= S3;
                        else
                            -- Cache miss
                            if ((initialized(set + 1) = '1' and dirty(set) = '1' and dirty(set + 1) = '0') or initialized(set + 1) = '0') then
                                mem_write <= '0';
                                mem_read <= '1';
                                
                                mem_addr <= address(11 downto 3);
                                
                                read_state <= S2;
                            elsif ((initialized(set + 1) = '1' and initialized(set) = '0') or (dirty(set) = '0' and dirty(set + 1) = '1')) then
                                tmp_cache(set) <= tmp_cache(set + 1);
                                tmp_tag(set) <= tmp_tag(set + 1);
                                initialized(set) <= initialized(set + 1);
                                dirty(set) <= dirty(set + 1);
                                
                                mem_write <= '0';
                                mem_read <= '1';
                                
                                mem_addr <= address(11 downto 3);
                                
                                read_state <= S2;
                            elsif (initialized(set) = '1' and dirty(set) = '1') then
                                -- start mem write of cache line
                                mem_read <= '0';
                                mem_write <= '1';
                                
                                -- address is still specifying current set
                                mem_addr <= tmp_tag(set) & address(4 downto 3);
                                mem_din <= tmp_cache(set);
                                
                                read_state <= S1;
                            else
                                if (lru(set) = '0') then
                                    tmp_cache(set) <= tmp_cache(set + 1);
                                    tmp_tag(set) <= tmp_tag(set + 1);
                                    initialized(set) <= initialized(set + 1);
                                    dirty(set) <= dirty(set + 1);
                                end if;
                                
                                mem_write <= '0';
                                mem_read <= '1';
                                
                                mem_addr <= address(11 downto 3);
                                
                                read_state <= S2;
                            end if;
                        end if;
                    else
                        -- stop read and writing if read_out or write_out is not 0
                        mem_read <= '0';
                        mem_write <= '0';
                    end if;
                
                -- --------------------------------------- --
                -- state 1: wait for writeback before read --
                -- --------------------------------------- --
                when S1 =>
                    if (write_out = '1') then
                        if (lru(set) = '0') then
                            tmp_cache(set) <= tmp_cache(set + 1);
                            tmp_tag(set) <= tmp_tag(set + 1);
                            initialized(set) <= initialized(set + 1);
                            dirty(set) <= dirty(set + 1);
                        end if;
                        
                        mem_write <= '0';
                        mem_read <= '1';
                        
                        mem_addr <= address(11 downto 3);
                        
                        read_state <= S2;
                    end if;
                
                -- --------------------------------------------------------- --
                -- state 2: copy memory output to set and output to data_out --
                -- --------------------------------------------------------- --
                when S2 =>
                    if (read_out = '1') then
                        tmp_cache(set + 1) <= mem_dout;
                        tmp_tag(set + 1) <= address(11 downto 5);
                        initialized(set + 1) <= '1';
                        dirty(set + 1) <= '0';
                        
                        case address(2 downto 0) is
                        when "000" =>
                            data_out <= mem_dout(127 downto 112);
                        when "001" =>
                            data_out <= mem_dout(111 downto 96);
                        when "010" =>
                            data_out <= mem_dout(95 downto 80);
                        when "011" =>
                            data_out <= mem_dout(79 downto 64);
                        when "100" =>
                            data_out <= mem_dout(63 downto 48);
                        when "101" =>
                            data_out <= mem_dout(47 downto 32);
                        when "110" =>
                            data_out <= mem_dout(31 downto 16);
                        when "111" =>
                            data_out <= mem_dout(15 downto 0);
                        end case;
                        
                        lru(set) <= '0';
                        lru(set + 1) <= '1';
                        
                        read_done <= '1';
                        
                        read_state <= S3;
                    end if;
                
                -- ------------------- --
                -- state 3: wait state --
                -- ------------------- --
                when S3 =>
                    read_done <= '1';
                end case;
            
            -- --------------- --
            -- Write to memory --
            -- --------------- --
            elsif (Mre = '0' and Mwe = '1') then
                -- Reset read state
                read_state <= S0;
                
                case write_state is
                -- ------------------------------------------- --
                -- state 0: initialize and check for cache hit --
                -- ------------------------------------------- --
                when S0 =>
                    if (read_out = '0' and write_out = '0') then
                        set := conv_integer(address(4 downto 3)) * 2;
                        mem_addr <= address(11 downto 3);
                        
                        -- Check for a cache hit
                        if (address(11 downto 5) = tmp_tag(set) and initialized(set) = '1') then
                            -- Set is a hit
                            dirty(set) <= '1';
                            
                            case address(2 downto 0) is
                            when "000" =>
                                tmp_cache(set) (127 downto 112) <= data_in;
                            when "001" =>
                                tmp_cache(set) (111 downto 96) <= data_in;
                            when "010" =>
                                tmp_cache(set) (95 downto 80) <= data_in;
                            when "011" =>
                                tmp_cache(set) (79 downto 64) <= data_in;
                            when "100" =>
                                tmp_cache(set) (63 downto 48) <= data_in;
                            when "101" =>
                                tmp_cache(set) (47 downto 32) <= data_in;
                            when "110" =>
                                tmp_cache(set) (31 downto 16) <= data_in;
                            when "111" =>
                                tmp_cache(set) (15 downto 0) <= data_in;
                            end case;
                            
                            lru(set) <= '1';
                            lru(set + 1) <= '0';
                            
                            write_done <= '1';
                            
                            write_state <= S3; -- wait state
                        elsif (address(11 downto 5) = tmp_tag(set + 1) and initialized(set + 1) = '1') then
                            -- Set + 1 is a hit
                            dirty(set + 1) <= '1';
                            
                            case address(2 downto 0) is
                            when "000" =>
                                tmp_cache(set + 1) (127 downto 112) <= data_in;
                            when "001" =>
                                tmp_cache(set + 1) (111 downto 96) <= data_in;
                            when "010" =>
                                tmp_cache(set + 1) (95 downto 80) <= data_in;
                            when "011" =>
                                tmp_cache(set + 1) (79 downto 64) <= data_in;
                            when "100" =>
                                tmp_cache(set + 1) (63 downto 48) <= data_in;
                            when "101" =>
                                tmp_cache(set + 1) (47 downto 32) <= data_in;
                            when "110" =>
                                tmp_cache(set + 1) (31 downto 16) <= data_in;
                            when "111" =>
                                tmp_cache(set + 1) (15 downto 0) <= data_in;
                            end case;
                            
                            lru(set) <= '0';
                            lru(set + 1) <= '1';
                            
                            write_done <= '1';
                            
                            write_state <= S3; -- wait state
                        else
                            -- Cache miss
                            if ((initialized(set + 1) = '1' and dirty(set) = '1' and dirty(set + 1) = '0') or initialized(set + 1) = '0') then
                                mem_write <= '0';
                                mem_read <= '1';
                                
                                mem_addr <= address(11 downto 3);
                                
                                write_state <= S2;
                            elsif (initialized(set) = '0' or dirty(set) = '0') then
                                tmp_cache(set) <= tmp_cache(set + 1);
                                tmp_tag(set) <= tmp_tag(set + 1);
                                initialized(set) <= initialized(set + 1);
                                dirty(set) <= dirty(set + 1);
                                
                                mem_write <= '0';
                                mem_read <= '1';
                                
                                mem_addr <= address(11 downto 3);
                                
                                write_state <= S2;
                            elsif (initialized(set) = '1' and dirty(set) = '1') then
                                -- start mem write of cache line
                                mem_read <= '0';
                                mem_write <= '1';
                                
                                -- address is still specifying current set
                                mem_addr <= tmp_tag(set) & address(4 downto 3);
                                mem_din <= tmp_cache(set);
                                
                                write_state <= S1;
                            else
                                mem_write <= '0';
                                mem_read <= '1';
                                
                                mem_addr <= address(11 downto 3);
                                
                                write_state <= S2;
                            end if;
                        end if;
                    else
                        mem_read <= '0';
                        mem_write <= '0';
                    end if;
                
                -- --------------------------------------- --
                -- state 1: wait for writeback before read --
                -- --------------------------------------- --
                when S1 =>
                    if (write_out = '1') then
                        if (lru(set) = '0') then
                            tmp_cache(set) <= tmp_cache(set + 1);
                            tmp_tag(set) <= tmp_tag(set + 1);
                            initialized(set) <= initialized(set + 1);
                            dirty(set) <= dirty(set + 1);
                        end if;
                        
                        mem_write <= '0';
                        mem_read <= '1';
                        
                        mem_addr <= address(11 downto 3);
                        
                        write_state <= S2;
                    end if;
                
                -- -------------------------------------------- --
                -- state 2: read from mem, write change to line --
                -- -------------------------------------------- --
                when S2 =>
                    if (read_out = '1') then
                        tmp_cache(set + 1) <= mem_dout;
                        tmp_tag(set + 1) <= address(11 downto 5);
                        initialized(set + 1) <= '1';
                        dirty(set + 1) <= '1';
                        
                        case address(2 downto 0) is
                        when "000" =>
                            tmp_cache(set + 1) (127 downto 112) <= data_in;
                        when "001" =>
                            tmp_cache(set + 1) (111 downto 96) <= data_in;
                        when "010" =>
                            tmp_cache(set + 1) (95 downto 80) <= data_in;
                        when "011" =>
                            tmp_cache(set + 1) (79 downto 64) <= data_in;
                        when "100" =>
                            tmp_cache(set + 1) (63 downto 48) <= data_in;
                        when "101" =>
                            tmp_cache(set + 1) (47 downto 32) <= data_in;
                        when "110" =>
                            tmp_cache(set + 1) (31 downto 16) <= data_in;
                        when "111" =>
                            tmp_cache(set + 1) (15 downto 0) <= data_in;
                        end case;
                        
                        lru(set) <= '0';
                        lru(set + 1) <= '1';
                        
                        write_done <= '1';
                        
                        write_state <= S3;
                    end if;
                
                -- ------------------- --
                -- state 3: wait state --
                -- ------------------- --
                when S3 =>
                    write_done <= '1';
                end case;
            
            -- ------------------------ --
            -- No write or read (Reset) --
            -- ------------------------ --
            else
                mem_read <= '0';
                read_done <= '0';
                
                read_state <= S0;
                write_state <= S0;
                
                mem_write <= '0';
                write_done <= '0';
            end if;
        end if;
    end if;
end process readwrite;

read_call <= mem_read;
write_call <= mem_write;

d_addr <= mem_addr;
d_word <= address(2 downto 0);

end rtl;
