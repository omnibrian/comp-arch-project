------------------------------------------------------------------
-- Simple n-bit counter
--
-- Creates an n-bit counter that can be reset when reset is '1'
-------------------------------------------------------------------

library ieee ;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity counter is
	generic (
		n : natural := 3
	);
	port (
		clock : in std_logic;
		reset : in std_logic;
		q     : out std_logic_vector(n-1 downto 0)
	);
end counter;

architecture behavior of counter is

signal pre_q: std_logic_vector(n-1 downto 0) := (others => '0');

begin

	process(clock, reset)
	begin
		if(reset = '1') then
			pre_q <= pre_q - pre_q;
		elsif(clock'event and clock = '1') then
			pre_q <= pre_q + 1;
		end if;
	end process;
	q <= pre_q;

end behavior;
