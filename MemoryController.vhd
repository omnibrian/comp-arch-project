------------------------------------------------------------------
-- Memory Controller
--
-- Controller for delaying memory
-- MemoryController.vhd
-------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;  
use ieee.numeric_std.all;			   
use work.MP_lib.all;

entity MemoryController is
port(
	clock			: in std_logic;
	reset			: in std_logic;
	Mre			: in std_logic;
	Mwe			: in std_logic;
	address		: in std_logic_vector(8 downto 0);
	data_in		: in std_logic_vector(127 downto 0);
	read_done	: out std_logic;
	write_done	: out std_logic;
	data_out		: out std_logic_vector(127 downto 0)
);
end MemoryController;

architecture rtl of MemoryController is

constant max: integer := 7;
signal read_counter: integer := max;
signal write_counter: integer := max;
signal mem_din: std_logic_vector(127 downto 0);
signal mem_dout: std_logic_vector(127 downto 0);

signal pll_c0: std_logic;

begin

Unit0: pll_clock port map(clock,pll_c0);
Unit1: memory_4k port map(address,mem_din,clock,pll_c0,Mre,Mwe,mem_dout);

read: process(clock, reset, Mre)
begin
	if reset = '1' then
		data_out <= std_logic_vector(to_unsigned(0,128));
		read_done <= '0';
		read_counter <= 0;
	else
		if (Mre = '1' and Mwe = '0') then
			if (clock'event and clock = '1') then
				if (read_counter = max - 1) then
					data_out <= mem_dout;
					read_counter <= read_counter + 1;
				elsif (read_counter = max) then
					data_out <= mem_dout;
					read_done <= '1';
				else
					read_counter <= read_counter + 1;
				end if;
			end if;
		else
			read_done <= '0';
			read_counter <= 0;
		end if;
	end if;
end process;

write: process(clock, reset, Mwe)
begin
	if reset = '1' then
		write_done <= '0';
		write_counter <= 0;
	else
		if (Mwe = '1' and Mre = '0') then
			if (clock'event and clock = '1') then
				if (write_counter = max - 1) then
					mem_din <= data_in;
					write_counter <= write_counter + 1;
				elsif (write_counter = max) then
					write_done <= '1';
				else
					write_counter <= write_counter + 1;
				end if;
			end if;
		else
			write_done <= '0';
			write_counter <= 0;
		end if;
	end if;
end process;

end rtl;