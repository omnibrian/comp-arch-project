# Computer Architecture Project #

Project repository for ECE3242 Computer Architecture implementing a Set-Associative Cache for SimpleCPU in VHDL.

### Instruction usage ###

(0x0) __mov1:__ `RF[rn] <= mem[direct]`

 1. __Op-code:__ "0000" (0x0) `(15 downto 12)`
 2. __Destination register:__ `(11 downto 8)`
 3. __Direct memory address:__ `(7 downto 0)`

(0x1) __mov2:__ `mem[direct] <= RF[rn]`

 1. __Op-code:__ "0001" (0x1) `(15 downto 12)`
 2. __Source register:__ `(11 downto 8)`
 3. __Destination memory address:__ `(7 downto 0)`

(0x2) __mov3:__ `mem[RF[rn]] <= RF[rm]`

 1. __Op-code:__ "0010" (0x2) `(15 downto 12)`
 2. __Memory addr register:__ `(11 downto 8)`
 3. __Source register:__ `(7 downto 4)`

(0x3) __mov4:__ `RF[rn] <= immediate`

 1. __Op-code:__ "0011" (0x3) `(15 downto 12)`
 2. __Destination register:__ `(11 downto 8)`
 3. __Immediate value:__ `(7 downto 0)`

(0x9) __mov5:__ `RF[rn] <= mem[RF[rm]]`

 1. __Op-code:__ "1001" (0x9) `(15 downto 12)`
 2. __Destination register:__ `(11 downto 8)`
 3. __Memory addr register:__ `(7 downto 4)`

(0x4) __add:__ `RF[rd] <= RF[r1] + RF[r2]`

 1. __Op-code:__ "0100" (0x4) `(15 downto 12)`
 2. __Source register 1:__ `(11 downto 8)`
 3. __Source register 2:__ `(7 downto 4)`
 4. __Destination register:__ `(3 downto 0)`

(0x5) __subt:__ `RF[rd] <= RF[r1] - RF[r2]`

 1. __Op-code:__ "0101" (0x5) `(15 downto 12)`
 2. __Source register 1:__ `(11 downto 8)`
 3. __Source register 2:__ `(7 downto 4)`
 4. __Destination register:__ `(3 downto 0)`

(0x8) __mult:__ `RF[rd] <= RF[r1] * RF[r2]`

 1. __Op-code:__ "1000" (0x8) `(15 downto 12)`
 2. __Source register 1:__ `(11 downto 8)`
 3. __Source register 2:__ `(7 downto 4)`
 4. __Destination register:__ `(3 downto 0)`

(0x6) __jz:__ `jump if RF[rn] = 0 to direct addr`

 1. __Op-code:__ "1011" (0xB) `(15 downto 12)`
 2. __Register:__ `(11 downto 8)`
 3. __Jump addr:__ `(7 downto 0)`

(0xF) __halt:__

 1. __Op-code: "1111" (0xF) (15 downto 12)

(0x7) __readm:__ `obuf <= mem[direct]`

 1. __Op-code:__ "0111" (0x7) `(15 downto 12)`
 2. __Memory addr:__ `(7 downto 0)`

(0xA) __readmi:__ `obuf <= mem[RF[rn]]`

 1. __Op-code:__ "1010" (0xA) `(15 downto 12)`
 2. __Memory addr register:__ `(3 downto 0)`

